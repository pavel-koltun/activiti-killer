import React from 'react';
import './Content.css';

function Content(props) {
    return (
        <div className="flexContainer flexItem content">
            <main className="flexItem main">
                <p>Appropriate content</p>
            </main>
        </div>
    );
}

export default Content;