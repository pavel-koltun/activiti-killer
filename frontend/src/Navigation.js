import React from 'react';
import './Navigation.css';

function Navigation(props) {
    return (
        <nav className="flexContainer grayBackground">
            <ul className="nav flexItem flexStart">
                <li className="title">Connected to: ...</li>
            </ul>
            <ul className="nav flexContainer flexEnd">
                <li><a href="#">Tasks</a></li>
                <li><a href="#">Processes</a></li>
                <li><a href="#">Connections</a></li>
            </ul>
        </nav>
    );
}

export default Navigation;