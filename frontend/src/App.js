import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Content from './Content';
import Navigation from './Navigation';

class App extends Component {
    state = {};

    render() {
        return (
            <div class="flexContainer flexColumn fullHeight whiteBackground">
                <Navigation />
                <Content />
            </div>
        );
    }
}

export default App;
