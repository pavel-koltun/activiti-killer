package by.koltun.controller;

import by.koltun.exception.TaskNotFoundException;
import org.activiti.engine.task.Task;
import org.activiti.rest.common.api.DataResponse;
import org.activiti.rest.service.api.runtime.task.TaskBaseResource;
import org.activiti.rest.service.api.runtime.task.TaskQueryRequest;
import org.activiti.rest.service.api.runtime.task.TaskResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

@RestController
@RequestMapping("/api/tasks")
public class TaskController extends TaskBaseResource {

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public DataResponse getTasks() {
        return filterTasks(TaskQueryRequest::new, Collections::emptyMap);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public DataResponse getTasksFiltered(@RequestBody final TaskQueryRequest request,
                                         @RequestParam(required = false) final Map<String,String> requestParameters) {
        return filterTasks(() -> request, () -> requestParameters);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskResponse getTask(@PathVariable("id") final String id) {
        final Task task = Optional.ofNullable(taskService.createTaskQuery().taskId(id).singleResult())
                .orElseThrow(TaskNotFoundException::new);
        return restResponseFactory.createTaskResponse(task);
    }

    private DataResponse filterTasks(final Supplier<TaskQueryRequest> taskQueryRequestSupplier,
                                     final Supplier<Map<String, String>> paginationPropertiesSupplier) {
        return getTasksFromQueryRequest(taskQueryRequestSupplier.get(), paginationPropertiesSupplier.get());
    }
}
