package by.koltun.controller;

import by.koltun.exception.ProcessInstanceNotFoundException;
import org.activiti.engine.ActivitiObjectNotFoundException;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.rest.common.api.DataResponse;
import org.activiti.rest.service.api.runtime.process.BaseProcessInstanceResource;
import org.activiti.rest.service.api.runtime.process.ProcessInstanceQueryRequest;
import org.activiti.rest.service.api.runtime.process.ProcessInstanceResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

@RestController
@RequestMapping("/api/process-instances")
public class ProcessInstanceController extends BaseProcessInstanceResource {

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public DataResponse queryProcessInstances() {
        return filterProcessInstances(ProcessInstanceQueryRequest::new, Collections::emptyMap);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public DataResponse queryProcessInstances(@RequestBody final ProcessInstanceQueryRequest request,
                                              @RequestParam(required = false) final Map<String,String> requestParameters) {
        return filterProcessInstances(() -> request, () -> requestParameters);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProcessInstanceResponse getProcessInstance(@PathVariable("id") final String id)
            throws ProcessInstanceNotFoundException {
        final ProcessInstance processInstance = Optional
                .ofNullable(runtimeService.createProcessInstanceQuery().processInstanceId(id).singleResult())
                .orElseThrow(ProcessInstanceNotFoundException::new);
        return restResponseFactory.createProcessInstanceResponse(processInstance);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteProcessInstance(@PathVariable("id") final String id, final HttpServletResponse response) {
        try {
            runtimeService.deleteProcessInstance(id, "Deleted from Activiti-Killer App");
        } catch (ActivitiObjectNotFoundException e) {
            throw new ProcessInstanceNotFoundException();
        }
        response.setStatus(HttpStatus.NO_CONTENT.value());
    }

    private DataResponse filterProcessInstances(final Supplier<ProcessInstanceQueryRequest> processQueryRequestSupplier,
                                                final Supplier<Map<String, String>> paginationPropertiesSupplier) {
        return getQueryResponse(processQueryRequestSupplier.get(), paginationPropertiesSupplier.get());
    }
}
